package com.anton.aidtest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
//import com.appsflyer.AFLogger;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerProperties;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private LocationManager mLocationManager;

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationReceiver(LocationManager.GPS_PROVIDER),
            new LocationReceiver(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // GPS
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }

//        AppsFlyerLib.getInstance().setDeviceTrackingDisabled(true);
        AppsFlyerLib.getInstance().setDebugLog(true);
        AppsFlyerLib.getInstance().setLogLevel(AFLogger.LogLevel.VERBOSE);

//        AppsFlyerLib.getInstance().setLogLevel(AFLogger.LogLevel.VERBOSE);


        AppsFlyerLib.getInstance().setUserEmails(AppsFlyerProperties.EmailsCryptType.SHA256,"test@appsflyer.com","test123@appsflyer.com\n");
//        AppsFlyerLib.getInstance().setUserEmails(AppsFlyerProperties.EmailsCryptType.SHA1,"test@appsflyer.com","test123@appsflyer.com\n");
//        AppsFlyerLib.getInstance().setUserEmails(AppsFlyerProperties.EmailsCryptType.MD5,"test@appsflyer.com","test123@appsflyer.com\n");
//        AppsFlyerLib.getInstance().setUserEmails("test@appsflyer.com","test123@appsflyer.com\n");
        AppsFlyerLib.getInstance().setCustomerUserId("TEST_CUSTOMER_ID");

        //setAdditionalData
//        Map<String,Object> map = new HashMap<>();
//        map.put("additinalData_1: ", new Integer(1));
//        map.put("additinalData_2: ", new Integer(2));
//        map.put("additinalData_3: ", new Integer(3));
//        AppsFlyerLib.getInstance().setAdditionalData((HashMap<String, Object>) map);

//        AppsFlyerLib.getInstance().setHostName("whappsflyer.com");


//        AppsFlyerLib.getInstance().setDebugLog(false);

//        AppsFlyerLib.getInstance().setHostName("whappsflyer.com");


//        AppsFlyerLib.getInstance().startTracking(this.getApplication(),"iErwCWL8p5wapsDjnDBQQb");

        String appsFlyerId = AppsFlyerLib.getInstance().getAppsFlyerUID(this);
        Log.d(AppsFlyerLib.LOG_TAG, "get appsflyer id call: " + appsFlyerId);

        AppsFlyerLib.getInstance().registerConversionListener(this, new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                for (String attrName : conversionData.keySet()) {
                    Log.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " +
                            conversionData.get(attrName));
                }
            }
            @Override
            public void onInstallConversionFailure(String errorMessage) {
                Log.d(AppsFlyerLib.LOG_TAG, "error getting conversion data: " +
                        errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {

            }

            @Override
            public void onAttributionFailure(String s) {

            }
        });

//        HashMap<String, Object> AdditionalData = new HashMap<>();
//        AdditionalData.put("1","Set Additional Data Test");
//        AppsFlyerLib.getInstance().setAdditionalData(AdditionalData);
//        AppsFlyerLib.getInstance().startTracking(this.getApplication(),"iErwCWL8p5wapsDjnDBQQb");
//        AppsFlyerLib.getInstance().setGCMProjectNumber(getApplicationContext(),"248066230410");
//        AppsFlyerLib.getInstance().setCurrencyCode("GBP");




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendDebugLog(){
//        AppsFlyerLib.getInstance().setDebugLog(true);
//        AppsFlyerLib.getInstance().trackEvent(this.getApplicationContext(),"iErwCWL8p5wapsDjnDBQQb",null);
    }

    public void inAppPurchase (View view) {
        Map<String,Object> eventData = new HashMap<>();
        eventData.put(AFInAppEventParameterName.CONTENT_ID, new String[] {"12344","98844","39944"});
        eventData.put(AFInAppEventParameterName.QUANTITY, new int[] {20, 11, 61});
        eventData.put(AFInAppEventParameterName.PRICE,new int[] {25, 50, 10});
        eventData.put(AFInAppEventParameterName.CURRENCY,"USD");
        eventData.put(AFInAppEventParameterName.REVENUE,99.54);
        eventData.put(AFInAppEventParameterName.CONTENT_TYPE,"ELECTRONIC");
        AppsFlyerLib.getInstance().trackEvent(this.getApplicationContext(), AFInAppEventType.PURCHASE,eventData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mLocationManager!=null) {
            mLocationManager.removeUpdates(mLocationListeners[0]);
            mLocationManager.removeUpdates(mLocationListeners[1]);
        }
    }


    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat
                    .requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT)
//                            .show();
                    registerLocationsReceiver();

                } else {
                    // Permission Denied
//                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT)
//                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void registerLocationsReceiver() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // GPS
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLocationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0l, 0l,
                mLocationListeners[0]);


        mLocationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0l, 0l,
                mLocationListeners[1]);
    }

}
