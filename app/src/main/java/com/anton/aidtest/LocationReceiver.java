package com.anton.aidtest;

import android.location.Location;
import android.os.Bundle;

/**
 * Created by anton on 30/10/2017.
 */

public class LocationReceiver implements android.location.LocationListener
    {
        Location mLastLocation;

    public LocationReceiver(String provider)
        {
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location)
        {
            mLastLocation.set(location);
        }

        @Override
        public void onProviderDisabled(String provider)
        {
        }

        @Override
        public void onProviderEnabled(String provider)
        {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
        }
    }