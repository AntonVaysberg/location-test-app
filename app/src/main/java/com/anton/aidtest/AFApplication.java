package com.anton.aidtest;

import android.app.Application;
import android.util.Log;


import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shacharaharon on 22/01/2017.
 */
public class AFApplication extends Application {

    private static final String TAG = "AFApplication";
    private static final String AF_DEV_KEY = "iErwCWL8p5wapsDjnDBQQb";
    public static final String TEST_APP_SHARED_PREF = "AFTestApp";
    private static ConversionCallbackUiHandler handler;
    private static DeeplinkCallbackUiHandler deeplinkHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        initAppsFlyerWithConversionListener(AF_DEV_KEY);

    }

    private void initAppsFlyerWithConversionListener(String afDevKey) {
//        AppsFlyerLib.getInstance().init(afDevKey, getConversionListener());
                AppsFlyerLib.getInstance().setLogLevel(AFLogger.LogLevel.VERBOSE);
        AppsFlyerLib.getInstance().startTracking(this, "iErwCWL8p5wapsDjnDBQQb");


//        AppsFlyerLib.getInstance().setMinTimeBetweenSessions(10);
    }

    private void initAppsFlyerWithConversionListenerAndStartTracking(String afDevKey) {
//        AppsFlyerLib.getInstance().init(afDevKey, getConversionListener()).startTracking(this);
    }

    public static void setConversionCallbackHandler(ConversionCallbackUiHandler handler) {
        AFApplication.handler = handler;
    }

    public static void setDeeplinkCallbackHandler(DeeplinkCallbackUiHandler deeplinkHandler) {
        AFApplication.deeplinkHandler = deeplinkHandler;
    }

    public interface ConversionCallbackUiHandler {
        void onInstallConversionDataLoaded(Map<String, String> conversionData);
        void onInstallConversionFailure(String errorMessage);
    }

    public interface DeeplinkCallbackUiHandler {
        void onAppOpenAttribution(Map<String, String> conversionData);
    }

    private AppsFlyerConversionListener getConversionListener() {
        return new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                if (handler != null) {
                    handler.onInstallConversionDataLoaded(conversionData);
                } else {
                    String res = "";
                    try { res = new JSONObject(conversionData).toString(); } catch (Throwable ignore) {}
                    Log.d(TAG, "onInstallConversionDataLoaded: handler was null.\nCouldn't parse conversion: "+res);
                }
            }

            @Override
            public void onInstallConversionFailure(String errorMessage) {
                if (handler != null) {
                    handler.onInstallConversionFailure(errorMessage);
                } else {
                    Log.d(TAG, "onInstallConversionFailure: handler was null.\nCouldn't parse conversion: "+errorMessage);
                }
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {
                if (deeplinkHandler != null) {
                    deeplinkHandler.onAppOpenAttribution(conversionData);
                } else {
                    String res = "";
                    try { res = new JSONObject(conversionData).toString(); } catch (Throwable ignore) {}
                    Log.d(TAG, "onAppOpenAttribution: handler was null.\nCouldn't parse conversion: "+res);
                }
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d(TAG, "Attribution error: " + errorMessage);
            }
        };
    }

}
